<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view('halaman.register');
    }
    
    public function welcomee(Request $request){
        $nama1 = $request->nama_depan;
        $nama2 = $request->nama_belakang;
        return view('halaman.welcomee', compact('nama1','nama2'));
    }
}
