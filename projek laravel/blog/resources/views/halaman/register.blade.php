    @extends('layout.master')

    @section('judul')
    Halaman Biodata
    @endsection

    @section('content')
        <form action="/kirim" method="POST">
            @csrf 
            <label>First name:</label> <br> <br>
            <input type="text" name="nama_depan"> <br> <br>
            <label>Last name:</label> <br> <br>
            <input type="text" name="nama_belakang"> <br> <br>
            <label>Gender:</label> <br> <br>
            <input type="radio" name="jk" value="gender">Male <br>
            <input type="radio" name="jk" value="gender">Female <br>
            <input type="radio" name="jk" value="gender">Other <br> 
            </select> <br> 
            <label>Nationality:</label> <br> <br>
            <select name="negara">
                <option value="1">Indonesia</option> 
            </select> <br> <br>
            <label>Language Spoken:</label> <br> <br>
            <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
            <input type="checkbox" name="bahasa">English <br>
            <input type="checkbox" name="bahasa">Other <br> <br>
            <label>Bio:</label> <br> <br>
            <textarea name="Bio" cols="30" rows="10"></textarea> <br>
            <input type="submit" value="Sign Up">
        </form>
    @endsection